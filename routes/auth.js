const express = require('express')
const router = express.Router()
const passport = require('../services/passport')

router.get('/twitter', passport.authenticate('twitter'))

router.get('/twitter/callback', passport.authenticate('twitter'), 
  function (req, res){
    const url = process.env.NODE_ENV === 'production' ? '/tasks' : 'http://127.0.0.1:3000/tasks'
    res.redirect(url)
  }
)

module.exports = router