const express = require('express')
const router = express.Router()
const requireLogin = require('../middlewares/requireLogin')
const mongoose =require('mongoose')
const Task = require('../models/Task')
const taskServices = require('../services/tasks')
mongoose.Promise = global.Promise

router.get('/api/tasks', requireLogin, async (req, res) => {
  try {
    const tasks = await Task.find({ _user: req.user._id })
    if (!tasks) return res.status(422).send({message: 'No hay tareas'}) 
    res.send(tasks)
  } catch (error) {
    res.status(422).send(error)
  }
})

router.get('/api/tasks/:id', requireLogin, async (req, res) => {
  try {
    const task = await Task.find({ _id: req.params.id })
    if (!task) return res.status(422).send({message: 'Tarea no encontrada'}) 
    res.send(task)
  } catch (error) {
    res.status(422).send(error)
  }
})

router.delete('/api/tasks/:id', requireLogin, async (req, res) => {
  try {
    const task = await Task.findByIdAndRemove({_id: req.params.id})
    if (!task) return res.status(422).send({messsage: 'Tarea no encontrada'})
    res.status(200).send({message: 'Tasks deleted'})
  } catch (error) {
    res.status(422).send(error)
  }
})

router.post('/api/tasks', requireLogin, async (req, res) => {
  const { description, start_at, stop_at, restart_at } = req.body

  const task = new Task({
    description,
    start_at,
    stop_at,
    restart_at,
    _user: req.user._id
  })

  try {
    await task.save()
    res.send(task)
  } catch (error) {
    res.status(422).send(error)
  }
})

router.patch("/api/tasks/:id", requireLogin, async (req, res) => {
  const taskFields = {}
  const attributes = req.body.attributes
  for (const key in attributes) {
    taskFields[key] = attributes[key]
  }
  try {
    let task = await Task.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: taskFields },
      { new: true }
    )
    if (!task) return res.status(422).send({ message: "Tarea no encontrada" })
    res.json(task)
  } catch (error) {
    res.status(422).send(error)
  }
})

router.get("/api/tasks/:id/stop", requireLogin, async (req, res) => {
  const taskFields = {}
  taskFields.stop_at = new Date()

  try {
    const task = await Task.findById({ _id: req.params.id }).exec()
    if (!task) return res.status(422).send({ message: "Task not found" })
    const timeToCompare = task.restart_at ? task.restart_at : task.start_at
    const diffInSecs = taskServices.diffInSecs(
      timeToCompare,
      taskFields.stop_at
    )
    taskFields.duration = task.duration + diffInSecs
  } catch (error) {
    return res.status(422).send(error)
  }

  try {
    let taskUpdate = await Task.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: taskFields },
      { new: true }
    )
    res.send(taskUpdate)
  } catch (error) {
    res.status(422).send(error)
  }
})

router.get("/api/tasks/:id/restart", requireLogin, async (req, res) => {
  const taskFields = {}
  taskFields.restart_at = new Date()
  taskFields.stop_at = null

  try {
    let task = await Task.findByIdAndUpdate({_id: req.params.id}, { $set: taskFields}, {new: true})
    if (!task) return res.status(422).send({ message: "Task not found" })
    res.json(task)
  } catch (error) {
    return res.status(422).send(error)
  }
})

router.get('/api/current_task', requireLogin, async (req, res)  => {
  const currentDate = new Date()

  try {
    const task = await Task.findOne({_user: req.user._id, stop_at: null}).exec()
    if (!task) return res.status(422).send({ message: "No current task" })
    res.json(task)
  } catch (error) {
    res.status(422).send(error)
  }
})

module.exports = router