const express = require('express')
const router = express.Router()

router.get('/api/current_user', (req, res) => {
  res.send(req.user)
})

router.get('/api/logout', (req, res) => {
  req.logout()
  req.session = null
  res.redirect('/')
})

module.exports = router