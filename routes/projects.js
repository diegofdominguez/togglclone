const express = require('express')
const router = express.Router()
const requireLogin = require('../middlewares/requireLogin')
const mongoose =require('mongoose')
const Project = require('../models/Project')
mongoose.Promise = global.Promise

router.get('/api/projects', requireLogin, async (req, res) => {
  try {
    const projects = await Project.find({ _user: req.user._id })
    if (!projects) return res.status(422).send({message: 'No projects found'}) 
    res.send(projects)
  } catch (error) {
    res.status(422).send(error)
  }
})

router.post('/api/projects', requireLogin, async (req, res) => {
  const { name } = req.body
  const project = new Project({
    name,
    _user: req.user._id
  })

  try {
    await project.save()
    res.send(project)
  } catch (error) {
    res.status(422).send(error)
  }
})

router.patch("/api/projects/:id", requireLogin, async (req, res) => {
  const projectFields = {}
  const attributes = req.body.attributes

  for (const key in attributes) {
    projectFields[key] = attributes[key]
  }

  try {
    let project = await Project.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: projectFields },
      { new: true }
    )
    if (!project) return res.status(422).send({ message: "Project not found" })
    res.json(project)
  } catch (error) {
    res.status(422).send(error)
  }
})

module.exports = router