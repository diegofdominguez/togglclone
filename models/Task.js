const mongoose = require('mongoose')
const { Schema } = mongoose

const TaskSchema = new Schema(
  {
    description: String,
    start_at: Date,
    stop_at: Date,
    restart_at: Date,
    duration: { type: Number, default: 0 },
    _user: { type: Schema.Types.ObjectId, ref: "User" },
    _project: { type: Schema.Types.ObjectId, ref: "Project" }
  },
  { timestamps: true }
)

module.exports = mongoose.model('tasks', TaskSchema)