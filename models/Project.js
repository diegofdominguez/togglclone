const mongoose = require('mongoose')
const { Schema } = mongoose

const ProjectSchema = new Schema(
  {
    name: String,
    _user: { type: Schema.Types.ObjectId, ref: "user" },
  },
  { timestamps: true }
)

module.exports = mongoose.model('project', ProjectSchema)