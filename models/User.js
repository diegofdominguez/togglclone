const mongoose = require('mongoose')
const { Schema } = mongoose

const UserSchema = new Schema(
  {
    twitterID: String,
    name: String
  },
  { timestamps: true }
);

module.exports = mongoose.model('users', UserSchema)