import React, { Component } from 'react'
import { connect } from 'react-redux'

class Landing extends Component  {
  render() {
    let button = null
    if (this.props.auth) {
      button = <a className="waves-effect waves-light btn-large" href="/tasks">My tasks</a>
    }
    return (
      <div style={{ textAlign: 'center' }}>
        <h1>TimerTrackApp</h1>
        <h4>Give it a try!</h4>
        {button}
      </div>
    )
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(Landing)