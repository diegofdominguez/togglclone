import React, { Component } from 'react'
import TasksList from './tasks/TasksList'
import { connect } from 'react-redux'

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timerText: "00:00:00",
      buttonText: "➤",
      timerRunning: false,
      timer: function() {},
      seconds: 0,
      existingTask: null
    };
  }

  timerHandler() {
    const newText = this.state.buttonText === "➤" ? "◼︎" : "➤";
    this.setState({ buttonText: newText });
    this.startTimer();
  }

  async startTimer() {
    // @ts-ignore
    clearInterval(this.state.timer);
    await this.setState({ timerRunning: !this.state.timerRunning });
    if (!this.state.timerRunning) return;
    this.setState({
      // @ts-ignore
      timer: setInterval(() => {
        this.setState({ seconds: this.state.seconds + 1 });
        this.displayTimerText();
      }, 1000)
    });
    this.displayTimerText();
  }

  stopTimer() {
    this.setState({ timerText: "00:00:00", seconds: 0, buttonText: "➤" });
  }

  displayTimerText() {
    const sec_num = this.state.seconds; // don't forget the second param
    const hours = Math.floor(sec_num / 3600);
    const minutes = Math.floor((sec_num - hours * 3600) / 60);
    const seconds = sec_num - hours * 3600 - minutes * 60;

    const hoursDisplay = hours < 10 ? "0" + hours : hours;
    const minutesDisplay = minutes < 10 ? "0" + minutes : minutes;
    const secondsDisplay = seconds < 10 ? "0" + seconds : seconds;

    const text = hoursDisplay + ":" + minutesDisplay + ":" + secondsDisplay;
    this.setState({ timerText: text });
  }

  startButton = {
    backgroundColor: "#99ff66",
    display: "inline",
    padding: "5px",
    color: "white"
  };
  
  stopButton = {
    backgroundColor: "#990033",
    display: "inline",
    padding: "5px",
    color: "white"
  }
  
  timerLEd = {
    display: "inline",
    padding: "5px"
  }

  timerBox = {
    margin: "20px auto"
  }

  descriptionInput = {
    width: "400px",
    fontSize: "20px"
  }

  setCurrentTask = async (taskId) => {
    await this.setState({existingTask: taskId})
    alert("Nuevo id" + this.state.existingTask)
  }

  setTaskDescription = (taskDescription) => {
    alert("Vamos bien")
  }

  render() {
    let content = null
    if (this.props.auth) {   
      return (
        <div className="container">
          <div className="container" id="timer-box" style={this.timerBox}>
            <input type="text" placeholder="What are you doing?" style={this.descriptionInput} />
            <h4 style={this.timerLEd}>{this.state.timerText}</h4>
            <h4
              style={
                !this.state.timerRunning ? this.startButton : this.stopButton
              }
              onClick={this.timerHandler.bind(this)}
            >
              {this.state.buttonText}
            </h4>
          </div>
          <TasksList click={this.setCurrentTask} descriptionChange={this.setTaskDescription} />
        </div>
      );
    } else {
      return (
        <h1>Please login</h1>
      )
    }
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(Dashboard) 