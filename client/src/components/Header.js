import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class Header extends Component {
  pickContent() {
    switch (this.props.auth) {
      case null:
        return 'Still deciding'
      case false:
        return <li><a href="/auth/twitter">Log In with Twitter</a></li>
      default:
        return <li><a href="/api/logout">LogOut</a></li>
    }
  }

  render() {
    return (
      <nav>
        <div className="nav-wrapper">
          <div className="container">
            <Link
              to={ this.props.auth ? '/tasks' : '/' } 
              className="brand-logo">
              TimerTrackApp
            </Link>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              {this.pickContent()}
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(Header);