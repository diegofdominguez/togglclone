import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchTasks } from '../../actions';
import TaskInput from "./TaskInput"

class TasksList extends Component {

  descriptionText = null

  componentDidMount() {
    this.props.fetchTasks()
  }

  setDescriptionText = (text) => {
    this.descriptionText = text
  }

  onClickHandler(event) {
    this.props.click(event)
  }

  renderTaskRow = (task => {
    return <tr key={task._id}>
              <TaskInput valueInput={task.description} taskId={task._id} />
              <td>{task.duration.toString() + " secs" }</td>
              <td onClick={this.onClickHandler.bind(this, task._id)}>Change</td>
            </tr>
  }).bind(this)

  displayTasks() {
    let tasks = null
    debugger
    if (this.props.tasks) {
      tasks = this.props.tasks.map(this.renderTaskRow)
    } 
    return (
      <tbody>
         {tasks}
      </tbody>
    )
  }

  render() {
    return (
      <div className="container">
        <table>{this.displayTasks()}</table>
      </div>
    )
  }
}

function mapStateToProps({tasks}) {
  return { tasks }
}

export default connect(mapStateToProps, { fetchTasks })(TasksList)