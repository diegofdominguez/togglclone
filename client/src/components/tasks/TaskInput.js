import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateTask } from '../../actions';

class TaskInput extends Component {
  state = {
    value: this.props.valueInput
  }

  onChangeHandler = (event) => {
    this.setState({value : event.target.value})
  }

  onKeyUpHandler(event) {
    if (event.key === 'Enter') {
      this.props.updateTask({"id": this.props.taskId, "attributes": { "description": this.state.value }})
    }
  }

  render() {
    return (
      <input key={this.props.taskId} type="text" value={this.state.value} 
        onChange={this.onChangeHandler}
        onKeyUp={this.onKeyUpHandler.bind(this)}
      />
    )
  }
}

function mapStateToProps({tasks}) {
  return { tasks }
}

export default connect(mapStateToProps, { updateTask })(TaskInput)

