export const FETCH_USER = "fetch_user"
export const FETCH_TASKS = 'fetch_tasks'
export const UPDATE_TASK = "update_task"
export const GET_CURRENT_TASK = "get_current_task"