import axios from 'axios'
import { FETCH_USER, FETCH_TASKS, UPDATE_TASK } from "./types";

export const fetchUser = () => async dispatch => {
  const res = await axios.get('/api/current_user')

  dispatch({ type: FETCH_USER, payload: res.data })
}

export const fetchTasks = () => async dispatch => {
  const res = await axios.get('/api/tasks');

  dispatch({ type: FETCH_TASKS, payload: res.data });
};

export const updateTask = (task) => async dispatch => {
  const res = await axios.patch('/api/tasks/' + task.id, {
    attributes: task.attributes
  })
}

export const getCurrentTask = () => async dispatch => {
  const res = await axios.get('/api/current_task')
}