import { FETCH_TASKS, UPDATE_TASK } from '../actions/types';

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_TASKS:
      return action.payload;
    case UPDATE_TASK:
      return action.payload;
    default:
      return state;
  }
}