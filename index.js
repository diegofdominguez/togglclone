const express = require('express')
const mongoose = require('mongoose')
const noCache = require('./middlewares/coCache')
const cookieSession = require('cookie-session')
const passport = require('passport')
const keys = require('./config/keys')
const authRoutes = require('./routes/auth')
const sessionsRoutes = require('./routes/sessions')
const tasksRoutes = require('./routes/tasks')
const projectsRoutes = require('./routes/projects')

mongoose.Promise = global.Promise
mongoose.connect(keys.mongodbURI)
const app = express()

app.use(noCache())
app.use(require('body-parser').json())
app.use(
  cookieSession({
    name: 'session',
    keys: [keys.cookieKey],
    maxAge: 30 * 24 * 60 * 60 * 1000
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use('/auth', authRoutes)
app.use('/', sessionsRoutes)
app.use('/', tasksRoutes)
app.use('/', projectsRoutes)

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'))
  const path = require('path')
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })
}

const PORT = process.env.PORT || 5000
app.listen(PORT)