const taskServices = {
  /**
   * Returns time difference in seconds
   * @param {Date} date1Str
   * @param {Date} date2Str
   * @returns {Number}
   */
  diffInSecs: (date1Str, date2Str) => {

    const diffInSecs = Math.floor((date2Str.getTime() - date1Str.getTime()) / 1000);
    return diffInSecs;
  },
  /**
   * Convert seconds in hh:mm:SS
   * @param {String} secondsStr
   */
  displaySecs: secondsStr => {
    const sec_num = parseInt(this, 10); // don't forget the second param
    const hours = Math.floor(sec_num / 3600);
    const minutes = Math.floor((sec_num - hours * 3600) / 60);
    const seconds = sec_num - hours * 3600 - minutes * 60;

    const hoursDisplay = hours < 10 ? "0" + hours : hours;
    const minutesDisplay = minutes < 10 ? "0" + minutes : minutes;
    const secondsDisplay = seconds < 10 ? "0" + seconds : seconds;

    return hoursDisplay + ":" + minutesDisplay + ":" + secondsDisplay;
  }
};

module.exports = taskServices