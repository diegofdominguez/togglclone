const passport = require("passport")
const Strategy = require("passport-twitter").Strategy
const keys = require("./../config/keys")
const User = require("./../models/User")

passport.serializeUser((id, done) => {
  User.findById(id).then(user => {
    done(null, user)
  })
})

passport.deserializeUser((obj, done) => {
  done(null, obj)
})

passport.use(
  new Strategy(
    {
      consumerKey: keys.twitterConsumerKEY,
      consumerSecret: keys.twitterConsumerSECRET,
      callbackURL: "/auth/twitter/callback"
    },
    async (accessToken, refreshToken, profile, done) => {
      const existingUser = await User.findOne({ twitterID: profile.id })

      if (existingUser) {
        return done(null, existingUser)
      }

      const newUser = await new User({
        twitterID: profile.id,
        name: profile.displayName
      }).save()
      done(null, newUser)
    }
  )
)

module.exports = passport
